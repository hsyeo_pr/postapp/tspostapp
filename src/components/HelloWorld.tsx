import React, {useState} from 'react';
import {Text, View, Button} from 'react-native';

const HelloWorld = () => {
  const [count, setCount] = useState<number>(0);
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Text>Hello, world!</Text>
      <Button onPress={() => setCount(count + 1)} title="Click me!" />
      <Text>{count}</Text>
    </View>
  );
};
export default HelloWorld;
